class String
  def present?
    !(self.nil? || self.empty?)
  end
end