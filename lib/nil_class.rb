class NilClass
  def present?
    !self.nil?
  end
end