module CustomerReturnTrack
  class ReturnOrder
    attr_reader :order_id, :return_reasons

    def initialize(order_id, return_reasons)
      @order_id = order_id
      @return_reasons = return_reasons
    end

    def valid_inputs?
      @order_id.present? and @return_reasons.present?
    end

    def response
      valid_inputs? ? parse_return_reason : nil
    end

    def parse_return_reason
      @return_reasons.to_i >= 1000000?  [8] : @return_reasons.scan(/10*|x[1-5]|[2-9]/)
    end

    def csv_output(res = response)
      res.present? ? [@order_id, res.join(',')].join(',') : nil
    end

    def sql_output(res = response)
      res.present? ? "UPDATE modo_orderarticle SET retoure_reason = #{res.join(',')} WHERE id = #{@order_id};" : nil
    end
  end
end