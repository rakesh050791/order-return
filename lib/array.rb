class Array
  def present?
    self.length > 0
  end
end