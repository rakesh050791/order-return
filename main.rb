require_relative './file_parser'

class Main
  def self.calculate
    file_parser = CustomerReturnTrack::FileParser.new(ARGV[0] || "input.csv")
    puts 'Provided file is not a valid file' unless file_parser.valid_file?
    return_data = file_parser.parse_inputs
    puts 'Please find the output in OutPut.csv and OutPut.sql'
    puts 'Unprocessed data:'
    return_data.each {|invalid_input| puts invalid_input}
  end
end

Main.calculate