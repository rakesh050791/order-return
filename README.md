# Return Track App
Our customers give us reasons when they return articles. These reasons are important for statistics and to optimise the selection for the next box.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine to try out.

### Prerequisites
Assuming you have installed `git`, `ruby` and `rvm` or any other `ruby version manager` like `rbenv`.

#### [Sample Input file content:]
```
"1009077b-110e-4010-b211-527137fdc0c2",10
"100a77c5-b15f-b5df-5a67-5134fed5a219",14
```

#### Expected Output file content:
```
UPDATE modo_orderarticle SET retoure_reason = '10' WHERE id = '1009077b-110e-4010-b211-527137fdc0c2';
UPDATE modo_orderarticle SET retoure_reason = '1,4' WHERE id = '100a77c5-b15f-b5df-5a67-5134fed5a219';
```

## Installing and Running the program
To start clone the repo.
```
git clone git@bitbucket.org:rakesh050791/order-return.git
cd order-return
```

Go to directory `order-return`
```
cd order_return
```

Install bundler if you don't have. And run bundle install to install all gems required.
```
gem install bundler
bundle install
```

Next, exucute the script
```
ruby main.rb
```

## Author

**Rakesh Joshi**


## License

Feel free to copy/edit/use code anywhere :)