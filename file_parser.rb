Dir["./lib/**/*.rb"].each {|klass| require klass }

module CustomerReturnTrack
  class FileParser
    attr_accessor :name

    def initialize(name)
      @name = name
    end

    def valid_file?
      file_present? && File.read(name)
    end

    def parse_inputs
      parse
    end

    private

    def file_present?
      return false unless name
      File.file?(name)
    end

    def parse
      invalid_inputs = []
      File.open("out_put.csv", "w") do |out_csv_file|
        File.open("out_put.sql", "w") do |out_sql_file|
          IO.foreach(name) do |line|
            line = line.strip
            unless line.empty?
              order_id, order_response = line.split(',')
              order = CustomerReturnTrack::ReturnOrder.new(order_id, order_response)
              order_response = order.response
              if order_response.present?
                out_csv_file.puts order.csv_output(order_response)
                out_sql_file.puts order.sql_output(order_response)
              else
                invalid_inputs << line
              end
            end
          end
        end
      end
      invalid_inputs
    end

  end
end